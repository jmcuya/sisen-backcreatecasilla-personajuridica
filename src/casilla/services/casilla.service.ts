import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Inbox, InboxDocument } from '../schemas/inbox.schema';
import { User, UserDocument } from '../schemas/user.schema';
import { UserInbox, userInboxDocument } from '../schemas/user_inbox.schema';
import * as fs from 'fs';
import * as cryptoJS from 'crypto';
import sha256 from 'crypto-js/sha256';
import { CaptchaService } from './captcha.service';
import { IGenericResponse } from '../dto/generic';
import { HttpException, HttpStatus, Logger } from '@nestjs/common';
import { Constants } from '../common/constants';
import { Representative, RepresentativeDocument } from '../schemas/representative.schema';
import { PersonTypeEnum } from '../common/enums/personTypeEnum';
import { RepresentativeService } from './representative.service';

export class CasillaService {
  private readonly logger = new Logger(CasillaService.name);

  constructor(
    @InjectModel(User.name)
    private userDocument: Model<UserDocument>,
    @InjectModel(Inbox.name)
    private inboxDocument: Model<InboxDocument>,
    @InjectModel(UserInbox.name)
    private userInboxDocument: Model<userInboxDocument>,
    @InjectModel(Representative.name)
    private representativeDocument: Model<RepresentativeDocument>,
    private representativeService: RepresentativeService,
    private captchaService: CaptchaService,
  ) {}

  saludar(nombre: string) {
    return 'hola ' + nombre;
  }

  async createBox(data: any, files) {
    const response: IGenericResponse<any> = {
      success: false,
    };

    data.personType = data.personType.toLowerCase();
    data.docType = data.docType.toLowerCase();
    data.email = data.email.toLowerCase();

    let userId = '';
    let inboxId = '';
    let oFileDocument = {};
    let oFilePhoto = {};

    try {
      const pass = cryptoJS.randomBytes(5).toString('hex');
      const personNatural = { name: '', lastname: '', second_lastname: '' };
      let organizationName = '';
      let webSite = '';
      let photoDoc = '';
      let rep: any;
      let fileDocument: any;

      const filePhoto = files.filePhoto[0];
      if (filePhoto == undefined) {
        throw new HttpException('Falta uno o varios archivos adjuntos', HttpStatus.BAD_REQUEST);
      }

      if (!this.validFile(filePhoto)) {
        response.message = 'Archivo imagen de DNI está dañado o no es válido';
        return response;
      }

      if (data.personType === PersonTypeEnum.PJ) {
        rep = JSON.parse(data.rep);
        organizationName = data.organizationName;
        webSite = data.webSite;
        photoDoc = rep.doc;

        fileDocument = files.fileDocument[0];
        if (fileDocument == undefined) {
          throw new HttpException('Falta uno o varios archivos adjuntos', HttpStatus.BAD_REQUEST);
        }

        if (!this.validFile(fileDocument)) {
          response.message = 'Archivo pdf de la resolución está dañado o no es válido';
          return response;
        }
      }

      if (data.personType === PersonTypeEnum.PN) {
        personNatural.name = data.name;
        personNatural.lastname = typeof data.lastname !== 'undefined' ? data.lastname : '';
        personNatural.second_lastname = typeof data.second_lastname !== 'undefined' ? data.second_lastname : '';
        photoDoc = data.doc;

        if (personNatural.lastname == '' && personNatural.second_lastname == '') {
          response.message = 'Ingrese por lo menos un apellido';
          return response;
        }
      }

      const resultUser = await new this.userDocument({
        doc: data.doc,
        doc_type: data.docType,
        profile: Constants.PROFILE_CITIZEN,
        password: pass,
        name: personNatural.name,
        lastname: personNatural.lastname,
        second_lastname: personNatural.second_lastname,
        email: data.email,
        cellphone: data.cellphone,
        phone: data.phone,
        Ubigeo: data.ubigeo,
        address: data.address,
        PaginaWeb: webSite,
        organization_name: organizationName,
        register_user_id: '',
        created_at: Date.now(),
        updated_password: false,
        create_user: Constants.USER_OWNER,
        status: Constants.STATUS_PENDING,
      }).save();

      if (!resultUser) {
        response.message = 'Sus datos no han sido guardados correctamente, por favor inténtelo más tarde.';
        return response;
      }

      userId = resultUser._id;

      // if(!respuestaUsuario) return {status :false , mensaje :'Sus datos no han sido guardados correctamente, por favor inténtelo más tarde.'}
      // var img = await this.copyFile(FileDni.buffer,'box/',FileDni.originalname,data.numeroDocumento,Date.now(),false,false);
      // if (typeof img != "object") return {status :false , mensaje :'Sus datos no han sido guardados correctamente, por favor inténtelo más tarde.'}

      oFilePhoto = await this.copyFile(
        filePhoto.buffer,
        'box/',
        filePhoto.originalname,
        photoDoc,
        Date.now(),
        false,
        false,
      );

      if (typeof oFilePhoto != 'object') {
        response.message = 'Sus datos no han sido guardados correctamente, por favor inténtelo más tarde.';
        return response;
      }

      // let id_usuario =  respuestaUsuario._id;
      const resultInbox = await new this.inboxDocument({
        doc: data.doc,
        doc_type: data.docType,
        email: data.email,
        cellphone: data.cellphone,
        phone: data.phone,
        address: data.address,
        acreditation_type: '',
        attachments: null,
        imageDNI: data.personType === PersonTypeEnum.PN ? oFilePhoto : null,
        register_user_id: userId.toString(),
        created_at: Date.now(),
        create_user: Constants.USER_OWNER,
        status: Constants.STATUS_PENDING,
        user_id: userId,
      }).save();

      // if(!respuestaInbox) return {status :false , mensaje :'Error al guardar casilla'}
      //  let id_inbox =  respuestaInbox._id;

      if (!resultInbox) {
        response.message = 'Error al guardar casilla';
        return response;
      }

      inboxId = resultInbox._id;
      // var respuestaUserInbox = await new this.userInboxDocument({
      //   doc : data.numeroDocumento,
      //   doc_type :data.tipoDocumento,
      //   profile: "owner",
      //   user_id : id_usuario,
      //   inbox_id : id_inbox
      // }).save();
      //
      // if(!respuestaUserInbox) return {status :false , mensaje :'Error al guardar usuario-casilla'}

      if (data.personType === PersonTypeEnum.PJ) {
        rep.docType = rep.docType.toLowerCase();
        rep.email = rep.email.toLowerCase();

        //save file document
        oFileDocument = await this.copyFile(
          fileDocument.buffer,
          'box/',
          fileDocument.originalname,
          rep.doc,
          Date.now(),
          false,
          false,
        );

        if (typeof oFileDocument != 'object') {
          response.message = 'Sus datos no han sido guardados correctamente, por favor inténtelo más tarde.';
          return response;
        }

        const resultSaveRep = await this.representativeService.save(rep, oFileDocument, oFilePhoto, userId, inboxId);

        if (!resultSaveRep) {
          response.message = 'Sus datos no han sido guardados correctamente, por favor inténtelo más tarde.';
          return response;
        }
      }

      // return {
      //   status: true,
      //   mensaje: 'success',
      // };
      response.success = true;
      response.message = 'Solicitud enviada';
      return response;
    } catch (err) {
      this.logger.error(
        JSON.stringify({
          message: 'error: create solicitude inbox',
          result: err,
        }),
      );
      response.message = err.message;
    }

    return response;
  }

  validFile(file) {
    const signedFile = file.buffer;
    if (this.validatebyteFile(file.mimetype, signedFile)) {
      return true;
    }
    return false;
  }

  validatebyteFile(typeFile, signedFile) {
    switch (typeFile) {
      case 'application/pdf':
        return (
          Buffer.isBuffer(signedFile) && signedFile.lastIndexOf('%PDF-') === 0 && signedFile.lastIndexOf('%%EOF') > -1
        );
      case 'image/jpg':
      case 'image/jpeg':
        return /^(ffd8ffe([0-9]|[a-f]){1}$)/g.test(signedFile.toString('hex').substring(0, 8));
      case 'image/png':
        return signedFile.toString('hex').startsWith('89504e47');
      case 'image/bmp':
      case 'image/x-ms-bmp':
        return signedFile.toString('hex').startsWith('424d');
      default:
        return false;
    }
  }

  stringHash(text) {
    return cryptoJS.createHash('sha256').update(text).digest('hex');
  }

  getPath(prePath) {
    const _date = new Date(Date.now());
    const retorno = prePath + _date.getFullYear() + '/' + (_date.getMonth() + 1) + '/' + _date.getDate() + '/';

    return retorno;
  }

  async copyFile(Buffer, newPath, filename, doc, timestamp, isTmp, isBlocked) {
    const path_upload = process.env.PATH_UPLOAD;
    const path_upload_tmp = process.env.PATH_UPLOAD_TMP;
    try {
      const rawData = Buffer; //fs.readFileSync(oldPathFile);
      const pathAttachment = this.getPath(newPath);

      const stringHashNameFile = this.stringHash(
        cryptoJS.randomBytes(5).toString('hex') + '_' + doc + '_' + timestamp + '_' + filename,
      );

      const newPathFile = (isTmp ? path_upload_tmp : path_upload) + '/' + pathAttachment + stringHashNameFile;

      fs.mkdirSync((isTmp ? path_upload_tmp : path_upload) + '/' + pathAttachment, { recursive: true });

      fs.writeFileSync(newPathFile, rawData);

      return { path: pathAttachment + stringHashNameFile, name: filename, blocked: isBlocked };
    } catch (err) {
      console.log('ERROOOOOORR', err);
      return false;
    }
  }
}
