/**
 * Created by Alexander Llacho
 */

export const Constants = {
  PROFILE_CITIZEN: 'citizen',
  STATUS_PENDING: 'PENDIENTE',
  USER_OWNER: 'owner',
};
