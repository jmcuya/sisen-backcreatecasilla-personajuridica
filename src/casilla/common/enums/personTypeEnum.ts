/**
 * Created by Alexander Llacho
 */

export enum PersonTypeEnum {
  PN = 'pn',
  PJ = 'pj',
}
