export const MESSAGE = {
  EXIST_INBOX: 'Usted ya cuenta con Casilla electrónica ingrese aquí <a href="https://casillaelectronica.onpe.gob.pe/#/login">https://casillaelectronica.onpe.gob.pe/#/login</a> y haga clic en "Olvidé mi contraseña"',
  EXIST_SOLICITUDE: 'Usted ya cuenta con una solicitud de registro de casilla electrónica pendiente de aprobación',
  EXIST_MORE_SOLICITUDE: 'Usted ya cuenta con más de una solicitud de registro de casilla electrónica pendiente de aprobación',
};