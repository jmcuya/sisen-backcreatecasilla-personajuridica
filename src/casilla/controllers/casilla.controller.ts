import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Ip,
  Logger,
  Post,
  Res,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { CasillaService } from '../services/casilla.service';
import { ResponseValidateData } from '../dto/ObtenerDatosPersonaDniResultDto';
import { responseSunat } from '../dto/ObtenerDatosSUNAT';
import { CiudadanoService } from '../services/ciudadano.service';
import { extname } from 'path';
import { CreateInboxRequest } from '../dto/CreateInboxRequest';
import { CaptchaService } from '../services/captcha.service';

export const multerOptions = {
  limits: {
    fileSize: 5242880,
  },
  fileFilter: (req: any, file: any, cb: any) => {
    type validMimeType = 'image/png' | 'image/jpg' | 'image/jpeg';
    const validMimeTypes: validMimeType[] = ['image/png', 'image/jpg', 'image/jpeg'];
    if (validMimeTypes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
    }
  },
};

@Controller()
export class CasillaController {
  private readonly logger = new Logger(CasillaController.name);

  constructor(
    private readonly casillaService: CasillaService,
    private readonly ciudadaoService: CiudadanoService,
    private readonly recaptchaService: CaptchaService,
  ) {}

  @Post('create-box')
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'filePhoto', maxCount: 1 },
        { name: 'filerepresent', maxCount: 2 },
      ],
      multerOptions,
    ),
  )
  async createBox(
    @Body() dto: CreateInboxRequest,
    @UploadedFiles() files: { filePhoto?: any; filerepresent?: any },
    @Ip() ipAddress,
    @Res() response,
  ): Promise<any> {
    try {
      if (Object.keys(files).length === 0) {
        throw new HttpException('No existe archivos adjuntos', HttpStatus.BAD_REQUEST);
      }

      if (process.env.DISABLED_RECAPTCHA !== 'true') {
        await this.recaptchaService.validarCapcha(dto.recaptcha, ipAddress);
      }

      const result = await this.casillaService.createBox(dto, files);
      //const result2 = await this.mpveService.enviarDocMesaPartes(dto.tipoDocumento, dto.numeroDocumento);
      return response.status(HttpStatus.OK).send(result);
    } catch (err) {
      this.logger.error(JSON.stringify(err));
      return response.status(err.status ? err.status : 500).send({ success: false, message: err.message });
    }
  }

  @Post('validarRUC')
  async validarPersonaJuridica(@Body() ruc: string): Promise<responseSunat> {
    return await this.ciudadaoService.validarDatosSUNAT(ruc);
  }
}
