import { Body, Controller, Ip, Post } from '@nestjs/common';
import { CodigoVerificacionService } from '../services/codigoVerificacion.service';
import { EnviarCorreoVerificacionDto } from '../dto/EnviarCorreoVerificacionDto';
import { ValidarCodigoDto } from '../dto/ValidarCodigoDto';
import { InboxService } from '../services/inbox.service';
import { CaptchaService } from '../services/captcha.service';

@Controller()
export class ValidacionCorreoController {
  constructor(
    private readonly codigoVerificacionService: CodigoVerificacionService,
    private readonly inboxService: InboxService,
    private readonly recaptchaService: CaptchaService,
  ) {}

  @Post('enviar-correo-verificacion')
  async enviarCorreoVerificacion(@Body() dto: EnviarCorreoVerificacionDto, @Ip() ipAddress) {
    /*try {
      await this.recaptchaService.validarCapcha(dto.recaptcha, ipAddress)
    } catch (err) {
      return { status: false, mensaje: 'Captcha inválido.' };
    }*/

    return await this.codigoVerificacionService.enviarCodigoVerificacion(
      dto.tipoDocumento.toLowerCase(),
      dto.numeroDocumento,
      dto.correoElectronico.toLowerCase(),
    );
  }

  @Post('validar-codigo-verificacion')
  async validarCodigo(@Body() dto: ValidarCodigoDto, @Ip() ipAddress) {
    /*try {
      if (process.env.DISABLED_RECAPTCHA !== 'true') {
        await this.recaptchaService.validarCapcha(dto.recaptcha, ipAddress);
      }
    } catch (err) {
      return { status: false, mensaje: 'Captcha inválido.' };
    }*/

    const valido = await this.codigoVerificacionService.validarCodigoVerificacion(
      dto.tipoDocumento.toLowerCase(),
      dto.numeroDocumento,
      dto.idEnvio,
      dto.codigo,
    );
    if (!valido) {
      console.log('El código de verificación es Incorrecto');
      return {
        esValido: false,
        mensaje: 'El código de verificación es Incorrecto',
      };
    }
    const existUserWithEmail = await this.inboxService.existeCasilleroConCorreo(
      dto.correo,
      dto.tipoDocumento.toLowerCase(),
      dto.personType,
    );
    if (existUserWithEmail) {
      console.log('El correo electrónico ya se encuentra en uso');
      return {
        esValido: false,
        mensaje: 'El correo electrónico ya se encuentra en uso',
      };
    }
    return {
      esValido: true,
      mensaje: 'El codigo fue validado exitosamente',
    };
  }
}
