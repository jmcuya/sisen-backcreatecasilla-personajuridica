import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Ip,
  Logger,
  Post,
  Res,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { InboxService } from '../services/inbox.service';
import { CaptchaService } from '../services/captcha.service';
import {
  InboxPersonLegalRequest,
  PersonLegalRequest,
  RepresentativeRequest,
  ValidateRepresentativeRequest,
  ValidPersonLegalRequest,
} from '../dto/personLegalRequest';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { extname } from 'path';
import { CiudadanoService } from '../services/ciudadano.service';
import { IGenericResponse } from '../dto/generic';
import { CasillaService } from '../services/casilla.service';
import { RepresentativeService } from '../services/representative.service';

export const multerOptions = {
  limits: {
    fileSize: 5242880,
  },
  fileFilter: (req: any, file: any, cb: any) => {
    type validMimeType = 'image/png' | 'image/jpg' | 'image/jpeg' | 'application/pdf';
    const validMimeTypes: validMimeType[] = ['image/png', 'image/jpg', 'image/jpeg', 'application/pdf'];
    if (validMimeTypes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
    }
  },
};

@Controller('/legal')
export class PersonLegalController {
  private readonly logger = new Logger(PersonLegalController.name);

  constructor(
    private readonly ciudadanoService: CiudadanoService,
    private readonly casillaService: CasillaService,
    private readonly inboxService: InboxService,
    private readonly representativeService: RepresentativeService,
    private readonly recaptchaService: CaptchaService,
  ) {}

  @Post('/person/data')
  async getPersonLegalByRUC(@Body() data: PersonLegalRequest, @Ip() ipAddress, @Res() response): Promise<any> {
    try {
      if (process.env.DISABLED_RECAPTCHA !== 'true') {
        await this.recaptchaService.validarCapcha(data.recaptcha, ipAddress);
      }

      const result = await this.ciudadanoService.getPersonLegalByRuc(data);
      // const result = await this.ciudadanoService.getPersonLegalByRucDB(data);

      return response.status(HttpStatus.OK).send(result);
    } catch (err) {
      this.logger.error(JSON.stringify(err));
      return response.status(err.status ? err.status : 500).send({ success: false, message: err.message });
    }
  }

  @Post('/person/validate')
  async validatePersonLegal(@Body() request: ValidPersonLegalRequest, @Ip() ipAddress, @Res() response): Promise<any> {
    const result: IGenericResponse<any> = {
      success: true,
    };

    request.docType = request.docType.toLowerCase();

    try {
      if (process.env.DISABLED_RECAPTCHA !== 'true') {
        await this.recaptchaService.validarCapcha(request.recaptcha, ipAddress);
      }

      // const result = await this.ciudadanoService.validPersonRUCDB(request);

      // if (result.success) {
      const existInbox = await this.inboxService.existeCasilleroConDoc(request.docType, request.doc);
      if (existInbox.exist) {
        result.success = false;
        result.message = existInbox.message;
      }
      // }

      return response.status(HttpStatus.OK).send(result);
    } catch (err) {
      this.logger.error(JSON.stringify(err));
      return response.status(err.status ? err.status : 500).send({ success: false, message: err.message });
    }
  }

  @Post('/representative/data')
  async getRepresentativeByDNI(@Body() data: RepresentativeRequest, @Ip() ipAddress, @Res() response): Promise<any> {
    const result: IGenericResponse<any> = {
      success: true,
    };

    try {
      if (process.env.DISABLED_RECAPTCHA !== 'true') {
        await this.recaptchaService.validarCapcha(data.recaptcha, ipAddress);
      }

      const resultData = await this.ciudadanoService.obtenerPersonaPorDni(data, ipAddress);
      // const resultData = await this.ciudadanoService.obtenerPersonaPorDniBd(data, ipAddress);

      if (resultData) {
        result.success = true;
        result.data = {
          name: resultData.nombres,
          lastname: resultData.apellidoPaterno,
          second_lastname: resultData.apellidoMaterno,
        };
      }

      return response.status(HttpStatus.OK).send(result);
    } catch (err) {
      this.logger.error(JSON.stringify(err));
      return response.status(err.status ? err.status : 500).send({ success: false, message: err.message });
    }
  }

  @Post('/representative/validate')
  async validateRepresentative(
    @Body() request: ValidateRepresentativeRequest,
    @Ip() ipAddress,
    @Res() response,
  ): Promise<any> {
    const result: IGenericResponse<any> = {
      success: true,
    };

    request.docType = request.docType.toLowerCase();

    try {
      if (process.env.DISABLED_RECAPTCHA !== 'true') {
        await this.recaptchaService.validarCapcha(request.recaptcha, ipAddress);
      }
      // const result = await this.personLegalService.validateRepresentativeDB(request);

      // if (result.success) {
      const existInbox = await this.representativeService.findByDoc({
        docType: request.docType,
        doc: request.doc,
        ruc: request.ruc,
      });
      if (!existInbox.success) {
        result.success = false;
        result.message = existInbox.message;
      }
      // }

      return response.status(HttpStatus.OK).send(result);
    } catch (err) {
      this.logger.error(JSON.stringify(err));
      return response.status(err.status ? err.status : 500).send({ success: false, message: err.message });
    }
  }

  @Post('/inbox/create-box')
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'fileDocument', maxCount: 1 },
        { name: 'filePhoto', maxCount: 1 },
      ],
      multerOptions,
    ),
  )
  public async createBox(
    @Body() data: InboxPersonLegalRequest,
    @UploadedFiles() files: { fileDocument?: any; filePhoto?: any },
    @Ip() ipAddress,
    @Res() response,
  ): Promise<any> {
    try {
      if (Object.keys(files).length === 0) {
        throw new HttpException('No existe archivos adjuntos', HttpStatus.BAD_REQUEST);
      }

      if (process.env.DISABLED_RECAPTCHA !== 'true') {
        await this.recaptchaService.validarCapcha(data.recaptcha, ipAddress);
      }

      const result = await this.casillaService.createBox(data, files);

      return response.status(HttpStatus.OK).send(result);
    } catch (err) {
      this.logger.error(JSON.stringify(err));
      return response.status(err.status ? err.status : 500).send({ success: false, message: err.message });
    }
  }
}
