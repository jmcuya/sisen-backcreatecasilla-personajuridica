export class PersonLegalResponse {
  docType: string;
  doc: string;
  name: string;
  type: string;
}

export class ValidDataResponse {
  status!: boolean;
  message!: string;
}
